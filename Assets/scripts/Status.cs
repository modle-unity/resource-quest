using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Status {
    public string name;
    public string labelValue;
    public int xOffsetMultiplier;
    public int quantity = 0;
    public Rect spriteRect = Rect.zero;
    public Rect labelRect = Rect.zero;
    public CustomFont font;
    float fontScale = 4f;
    private float xUnit;
    private float yUnit;
    private static Dictionary<string, int> posParams = new Dictionary<string, int>();
    public List<Texture2D> spriteTextures = new List<Texture2D>();


    public Status(string _name, string _labelValue, int _xOffsetMultiplier, Dictionary<string, int> _posParams) {
        name = _name;
        labelValue = _labelValue;
        xOffsetMultiplier = _xOffsetMultiplier;
        posParams = _posParams;

        if (name == "villagers") {
            font = new CustomFont(Color.black, fontScale);
            spriteTextures = VillagerHandler.handler.LoadSpriteList();
        } else {
            font = new CustomFont(Color.red, fontScale);
            spriteTextures.Add((Texture2D)Resources.Load(name));
        }
    }

    public void Update() {
        UpdatePosition();
        if (name == "villagers") {
            this.quantity = VillagerHandler.handler.villagers.Count;
            return;
        }
        if (name == "dead") {
            this.quantity = VillagerHandler.handler.deadCount;
            return;
        }
        this.quantity = 0;
        foreach (Villager entry in VillagerHandler.handler.villagers) {
            this.quantity += (entry.conditions.Contains(this.name) ? 1 : 0);
        }
    }

    private void UpdatePosition() {
        if (xUnit != 0 && yUnit != 0 && xUnit == Utils.utils.xUnit && yUnit == Utils.utils.yUnit) {
            return;
        }
        xUnit = Utils.utils.xUnit;
        yUnit = Utils.utils.yUnit;
        spriteRect.Set(
            posParams["statusX"] + xOffsetMultiplier * posParams["statusOffset"],
            posParams["statusY"] - yUnit * 2,
            xUnit * 3,
            xUnit * 3
        );
        labelRect.Set(
            posParams["statusX"] + xOffsetMultiplier * posParams["statusOffset"],
            spriteRect.y + spriteRect.height + yUnit * 2,
            1,
            1
        );
        font.Update();
    }

    public void Add(int _quantity) {
        this.quantity += _quantity;
    }

    public void Draw() {
        if (!State.state.showHelp) {
            int textureKey = Utils.utils.OscillateSprite(spriteTextures, spriteTextures.Count);
            GUI.DrawTexture(spriteRect, spriteTextures[textureKey]);
            GUI.Label(this.labelRect, GetText(), this.font.font);
        }
    }

    string GetText() {
        return Utils.utils.SquishNumber((float)this.quantity);
    }
}
