using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StatusHandler {
    public static Dictionary<string, Status> statuses = new Dictionary<string, Status>();
    private static Dictionary<string, int> posParams = new Dictionary<string, int>();
    private static float xUnit;
    private static float yUnit;

    static StatusHandler() {
        SetDefaults();
    }

    private static void SetDefaults() {
        UpdatePositionValues();
        GenerateStatuses();
    }

    public static void UpdatePositionValues() {
        if (xUnit != 0 && yUnit != 0 && xUnit == Utils.utils.xUnit && yUnit == Utils.utils.yUnit) {
            return;
        }
        xUnit = Utils.utils.xUnit;
        yUnit = Utils.utils.yUnit;
        SetPositionParams();
    }

    private static void SetPositionParams() {
        Debug.Log("updating position params in StatusHandler SetPositionParams");
        posParams["statusX"] = (int)(xUnit * 10);
        posParams["statusY"] = (int)(yUnit * 120);
        posParams["statusOffset"] = (int)(xUnit * 8);
    }

    private static void GenerateStatuses() {
        statuses["villagers"] = new Status("villagers", "placeholder", 0, posParams);
        statuses["starving"] = new Status("starving", "need food...", 1, posParams);
        statuses["freezing"] = new Status("freezing", "throw another on the fire", 2, posParams);
        statuses["toolless"] = new Status("toolless", "no tools remain", 3, posParams);
        statuses["naked"] = new Status("naked", "clothes are for chumps", 4, posParams);
        statuses["dead"] = new Status("dead", "probably your fault", 5, posParams);
    }

    public static void Manage() {
        UpdatePositionValues();
        foreach (KeyValuePair<string, Status> entry in statuses) {
            entry.Value.Update();
        }
    }

    public static void Draw() {
        foreach (KeyValuePair<string, Status> entry in statuses) {
            if (entry.Value.quantity > 0) {
                entry.Value.Draw();
            }
        }
    }

    public static void Reset() {
        SetDefaults();
    }
}
