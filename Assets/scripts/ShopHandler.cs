using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopHandler : MonoBehaviour {

    public static ShopHandler shop;

    public string text = "Shop coming soon";
    public Rect textBox = Rect.zero;
    public Rect textBoxOutline = Rect.zero;
    public Rect coinRect = Rect.zero;
    Rect baseEntryBox = Rect.zero;
    public Texture2D textBoxOutlineTexture;
    public Texture2D buyIcon;
    public CustomFont font;
    private float xUnit;
    private float yUnit;
    private float fontScale = 3.5f;
    public static Dictionary<string, ShopItem> shopItems = new Dictionary<string, ShopItem>();
    private static Dictionary<string, float> posParams = new Dictionary<string, float>();
    static int shopItemOffsetMultiplier = 40;

    void Awake() {
        // singleton pattern
        if (shop == null) {
            DontDestroyOnLoad(gameObject);
            shop = this;
        } else if (shop != this) {
            Destroy(gameObject);
        }
    }

    private void Start() {
        // I guess we got lucky with load order with Logs, here Utils is not available at Awake so we're using Start
        SetupTextBox();
        AddItems();
    }

    private void Update() {
        SetPositionValues();
    }

    private void SetPositionValues() {
        if (xUnit != 0 && yUnit != 0 && xUnit == Utils.utils.xUnit && yUnit == Utils.utils.yUnit) {
            return;
        }
        xUnit = Utils.utils.xUnit;
        yUnit = Utils.utils.yUnit;
        posParams["buttonX"] = xUnit * 7.5f;
        posParams["buttonY"] = yUnit * 5;
        textBox = new Rect(xUnit * 150, yUnit * 105, xUnit * 10, yUnit * 10);
        textBoxOutline = new Rect(textBox.x - xUnit, textBox.y - yUnit, xUnit * 40, Screen.height);
    }

    private void SetupTextBox() {
        font = new CustomFont(Color.black, fontScale);
        textBoxOutlineTexture = (Texture2D)Resources.Load("theTexture6");
        buyIcon = (Texture2D)Resources.Load("buy");
    }

    private void AddItems() {
        // public ShopItem(string _name, string _description, string _type, string _effect, string _target, int _cost) {
        shopItems.Add(
            "food",
            new ShopItem(
                "buy food",
                "description",
                "item",
                "food",
                "target",
                10
            )
        );
        shopItems.Add(
            "clothes",
            new ShopItem(
                "buy clothes",
                "description",
                "item",
                "clothing",
                "target",
                30
            )
        );
        shopItems.Add(
            "tools",
            new ShopItem(
                "buy tool",
                "description",
                "item",
                "tool",
                "target",
                50
            )
        );
        shopItems.Add(
            "firewood",
            new ShopItem(
                "buy firewood",
                "description",
                "item",
                "firewood",
                "target",
                50
            )
        );
    }

    public void Draw() {
        text = "PURCHASE";
        font.Update();
        GUI.DrawTexture(textBoxOutline, textBoxOutlineTexture);
        GUI.Label(textBox, text, font.font);
        HandleEntries();
    }

    private void HandleEntries() {
        int count = 0;
        foreach(KeyValuePair<string, ShopItem> entry in shopItems) {
            count += 1;
            entry.Value.UpdatePos(textBox, xUnit, yUnit, shopItemOffsetMultiplier, count);
            if (GUI.Button(entry.Value.purchaseRect, buyIcon)) {
                bool removed = Score.score.Remove(entry.Value.cost);
                if (removed) {
                    StoreHandler.handler.stores[entry.Value.effect].Add(1);
                }
                Debug.Log("item clicked " + entry.Value.name);
            }
            GUI.Label(entry.Value.costRect, entry.Value.cost.ToString(), font.font);
            GUI.DrawTexture(entry.Value.iconRect, StoreHandler.handler.textures[entry.Value.effect]);
            GUI.Label(entry.Value.labelRect, entry.Value.name, font.font);
            GUI.DrawTexture(entry.Value.coinRect, Score.score.GetCurrentTexture());
        }
    }
}
