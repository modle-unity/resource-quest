using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class DebugOverlay {
    public static Rect buildingsTextBox;
    public static Rect eventsTextBox;
    public static Rect storesTextBox;
    public static Rect utilsTextBox;
    public static Rect villagersTextBox;
    public static Rect resetEventsButton;
    public static Rect deleteSaveButton;
    public static CustomFont font;
    static int fontScale = 2;
    static float xUnit = Utils.utils.xUnit;
    static float yUnit = Utils.utils.yUnit;

    static DebugOverlay() {
        SetDefaults();
    }

    private static void SetDefaults() {
        buildingsTextBox = new Rect(xUnit * 1, xUnit * 60, xUnit * 80, yUnit * 10);
        eventsTextBox = new Rect(xUnit * 1, xUnit * 2, xUnit * 80, yUnit * 10);
        storesTextBox = new Rect(xUnit * 1, xUnit * 80, xUnit * 80, yUnit * 10);
        utilsTextBox = new Rect(xUnit * 65, xUnit * 80, xUnit * 80, yUnit * 10);
        villagersTextBox = new Rect(xUnit * 130, xUnit * 2, xUnit * 80, yUnit * 10);

        resetEventsButton = new Rect(xUnit * 20, xUnit * 2, xUnit * 20, yUnit * 5);
        deleteSaveButton = new Rect(xUnit * 50, xUnit * 2, xUnit * 20, yUnit * 5);

        font = new CustomFont(Utils.utils.textColorDebug, fontScale);
    }

    public static void Draw() {
        GUI.Label(villagersTextBox, VillagerHandler.handler.GetVillagersDebugRepr(), font.font);
        GUI.Label(eventsTextBox, EventHandler.handler.GetEventsDebugRepr(), font.font);
        GUI.Label(buildingsTextBox, BuildingHandler.handler.GetBuildingsDebugRepr(), font.font);
        GUI.Label(storesTextBox, StoreHandler.handler.GetStoresDebugRepr(), font.font);
        GUI.Label(utilsTextBox, Utils.utils.GetUtilsRepr(), font.font);

        if (GUI.Button(resetEventsButton, "RESET EVENTS")) {
            EventHandler.handler.Reset();
        }
        if (GUI.Button(deleteSaveButton, "DELETE SAVE")) {
            GameControl.control.PurgeSave();
        }
    }
}
