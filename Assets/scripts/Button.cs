using UnityEngine;

public class Button {
    public string name;
    public Rect rect = Rect.zero;
    public Texture2D texture;
    public System.Action Action;
    public string screen;
    float xUnit = Utils.utils.xUnit;
    float yUnit = Utils.utils.yUnit;
    int horizontalOffsetMultipler = 6;
    int offset;

    public Button(string theName, int _offset, ButtonActions buttonActions, string _screen) {
        name = theName;
        offset = _offset;
        texture = (Texture2D)Resources.Load("menu/" + name);
        Action = buttonActions.actions[name];
        screen = _screen;
    }

    public void Update(int x, int y, float width, float height) {
        rect.Set(
            x - width * offset - width / 5 * offset,
            y,
            width,
            height
        );
    }
}
