using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class GameControl : MonoBehaviour {

    // singleton pattern
    public static GameControl control;
    string filePath;
    int timeSinceSave;
    float timeBetweenSaves = 5;
    // lastSaveTime is used by save loop to see if new save is possible
    public float lastSaveTime;

    Rect textBox = Rect.zero;
    Rect textBoxOutline = Rect.zero;
    Texture2D textBoxOutlineTexture;
    CustomFont font;
    private int fontScale = 3;
    private float xUnit;
    private float yUnit;

    public bool uiLocked;
    public string uiTargetElement = "";

    void Awake() {
        // randomize name or check for existence
        filePath = Application.persistentDataPath + "/gameInfo10.dat";

        // singleton pattern
        if (control == null) {
            DontDestroyOnLoad(gameObject);
            control = this;
        } else if (control != this) {
            Destroy(gameObject);
        }
    }

    void Start() {
        EventHandler.handler.SetDefaults();
        VillagerHandler.handler.SetDefaults();
        BuildingHandler.handler.SetDefaults();
        Score.score.SetDefaults();
        StoreHandler.handler.SetDefaults();
        lastSaveTime = Utils.utils.currentGameTime;
        Load();
        SetupTextBox();
    }

    private void Update() {
        Log.log.Update();
        if (State.state.gameOver) {
            return;
        }
        UpdateTextBox();
        Score.score.Update();
        ButtonHandler.Manage();
        EventHandler.handler.Manage();
        VillagerHandler.handler.Manage();
        StatusHandler.Manage();
        if (VillagerHandler.handler.AllDead() && !State.state.gameOver) {
            State.state.gameOver = true;
            Log.log.Add("dead... all dead...");
            return;
        }
        BuildingHandler.handler.Manage();
        GameControl.control.Save();
    }

    // OnGUI is called twice(?) per frame
    void OnGUI() {
        BuildingHandler.handler.Draw();
        SpeedSlider.slider.Draw();
        VillagerHandler.handler.Draw();
        StoreHandler.handler.Draw();
        StatusHandler.Draw();
        Log.log.Draw();
        Score.score.Draw();
        ShopHandler.shop.Draw();
        ButtonHandler.Draw();
        EventHandler.handler.Draw();
        if (State.state.showHelp) {
            DebugOverlay.Draw();
        }
        Draw();
    }

    void SetupTextBox() {
        font = new CustomFont(Color.black, fontScale);
        textBoxOutlineTexture = (Texture2D)Resources.Load("theTexture6");
    }

    public void Restart() {
        BuildingHandler.handler.Reset();
        State.state.Reset();
        EventHandler.handler.Reset();
        StatusHandler.Reset();
        StoreHandler.handler.Reset();
        VillagerHandler.handler.Reset();
        Log.log.Reset();
        Score.score.Reset();
        SpeedSlider.slider.Reset();
        Utils.utils.Reset();
        lastSaveTime = 0;
    }

    public void Save() {
        if (!State.state.IsActive()) {
            return;
        }
        if (Utils.utils.currentGameTime - lastSaveTime < timeBetweenSaves) {
            return;
        }
        PrepareForSave();

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(filePath);
        GameData data = new GameData();
        // put currentSliderValue first; Events loading is dependent on it
        data.currentSliderValue = SpeedSlider.slider.sliderRect.x;
        data.score = Score.score.value;

        data.stores = StoreHandler.handler.values;
        data.produced = StoreHandler.handler.produced;
        data.consumed = StoreHandler.handler.consumed;

        data.villagerCount = VillagerHandler.handler.villagers.Count;
        data.deadVillagerCount = VillagerHandler.handler.deadCount;
        data.villagerStats = VillagerHandler.handler.GetVillagerStats();
        data.villagerAssignments = VillagerHandler.handler.GetVillagerAssignments();
        data.villagersActive = VillagerHandler.handler.active;

        data.logEntries = Log.log.entries;
        data.currentEvent = EventHandler.handler.currentEvent;
        data.gameOver = State.state.gameOver;
        data.lastSaveTime = Utils.utils.lastSaveTime;
        bf.Serialize(file, data);
        file.Close();
        // Log.log.Add("Game Saved.");
        Debug.Log("Game saved to " + filePath);
    }

    private void PrepareForSave() {
        // Log.log.Add("Saving game...\n");
        lastSaveTime = Utils.utils.currentGameTime;
        Utils.utils.lastSaveTime = lastSaveTime;
    }

    public void Load() {
        if (File.Exists(filePath)) {
            BinaryFormatter bf = new BinaryFormatter();

            FileStream file = File.Open(filePath, FileMode.Open);
            GameData data = (GameData)bf.Deserialize(file);
            file.Close();

            if (data.score > 0) {
                // can't use Log.log statements here because log gets overwritten
                Log.log.Add("Loading game...\n");
                Debug.Log("Loading game...\n");
                // put currentSliderValue first; Events loading is dependent on it
                SpeedSlider.slider.sliderRect.x = data.currentSliderValue;
                Score.score.value = data.score;

                StoreHandler.handler.values = data.stores;
                StoreHandler.handler.produced = data.produced;
                StoreHandler.handler.consumed = data.consumed;
                StoreHandler.handler.Load();

                VillagerHandler.handler.count = data.villagerCount;
                VillagerHandler.handler.deadCount = data.deadVillagerCount;
                VillagerHandler.handler.loadedStats = data.villagerStats;
                VillagerHandler.handler.loadedAssignments = data.villagerAssignments;
                VillagerHandler.handler.active = data.villagersActive;
                VillagerHandler.handler.Load();

                Log.log.entries.AddRange(data.logEntries);
                EventHandler.handler.currentEvent = data.currentEvent;
                State.state.gameOver = data.gameOver;

                Utils.utils.lastSaveTime = data.lastSaveTime;
                Utils.utils.loadedGameTime = data.lastSaveTime;
            }
            Log.log.Add("Loaded game.");
            Debug.Log("Loaded game save from path " + filePath);
        } else {
            Debug.Log("No save file found at expected path " + filePath);
        }
    }

    public void PurgeSave() {
        if (File.Exists(filePath)) {
            File.Delete(filePath);
            Debug.Log("Deleted save file at " + filePath);
        } else {
            Debug.Log("No save file found");
        }
    }

    private void UpdateTextBox() {
        if (xUnit != 0 && yUnit != 0 && xUnit == Utils.utils.xUnit && yUnit == Utils.utils.yUnit) {
            return;
        }
        xUnit = Utils.utils.xUnit;
        yUnit = Utils.utils.yUnit;
        textBox.Set(xUnit * 65, yUnit * 135, xUnit, yUnit);
        textBoxOutline.Set(textBox.x - xUnit, textBox.y - yUnit, xUnit * 30, Screen.height);
    }

    public void Draw() {
        string text = "Time since load: " + Time.time.ToString("F1") +
            "\nTotal game time: " + Utils.utils.currentGameTime.ToString("F1") +
            "\nLast load time: " + Utils.utils.loadedGameTime.ToString("F1") +
            "\nLast save time: " + Utils.utils.lastSaveTime.ToString("F1") +
            "\nLast rover time: " + VillagerHandler.handler.timeOfLastRover.ToString("F1") +
            "\nNext rover time: " + VillagerHandler.handler.timeToNext.ToString("F1") +
            "\nLast event end time: " + EventHandler.handler.lastEventEndTime.ToString("F1") +
            "\nNext event time: " + EventHandler.handler.timeToNextEvent.ToString("F1") +
            "\nCurrent event start time: " + EventHandler.handler.currentEvent.startTime.ToString("F1") +
            "\nCurrent event duration: " + EventHandler.handler.currentEvent.duration.ToString("F1") +
            "\nxUnit: " + Utils.utils.xUnit.ToString("F1") +
            "\nyUnit: " + Utils.utils.yUnit.ToString("F1") +
            "\nsliderValue: " + SpeedSlider.slider.currentSliderValue.ToString("F2");
        font.Update();
        GUI.DrawTexture(textBoxOutline, textBoxOutlineTexture);
        GUI.Label(textBox, text, font.font);
    }

    public bool IsUILockableByType(string checkType) {
        if (!uiLocked) {
            // if unlocked, go ahead
            return true;
        } else {
            // if locked, but locked by type or flag set to unlocked, return lockable true
            if (uiTargetElement == checkType || uiTargetElement == "unlocked") {
                return true;
            }
        }
        // not lockable by default
        return false;
    }

    public bool LockUIByType(string lockType) {
        bool isLockable = IsUILockableByType(lockType);
        if (isLockable) {
            uiTargetElement = lockType;
            uiLocked = true;
            return true;
        } else {
            return false;
        }
    }

    public void UnlockUI(string lockType) {
        if (uiTargetElement == lockType) {
            uiTargetElement = "unlocked";
            uiLocked = false;
        }
    }
}

[Serializable]
class GameData {
    // put currentSliderValue first; Events loading is dependent on it
    public float currentSliderValue;
    public int score;
    public Dictionary<string, float> stores = new Dictionary<string, float>();
    public Dictionary<string, int> produced = new Dictionary<string, int>();
    public Dictionary<string, int> consumed = new Dictionary<string, int>();
    public int villagerCount;
    public int deadVillagerCount;
    public List<Dictionary<string, Stat>> villagerStats = new List<Dictionary<string, Stat>>();
    public List<string> villagerAssignments = new List<string>();
    public bool villagersActive;
    public List<string> logEntries = new List<string>();
    public Event currentEvent;
    public bool gameOver;
    public float lastSaveTime;
}