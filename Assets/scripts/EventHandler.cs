using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventHandler : MonoBehaviour {

    public static EventHandler handler;

    public List<Event> events;
    public Event currentEvent;
    public float lastEventEndTime = 0f;
    public float timeToNextEvent;

    bool activated = false;
    CustomFont font;
    float fontScale = 4.0f;
    int minTimeBetweenEvents = 15;
    int maxTimeBetweenEvents = 30;
    Rect textBox = Rect.zero;
    float xUnit;
    float yUnit;

    void Awake() {
        // singleton pattern
        if (handler == null) {
            DontDestroyOnLoad(gameObject);
            handler = this;
        } else if (handler != this) {
            Destroy(gameObject);
        }
    }

    public void SetDefaults() {
        font = new CustomFont(Color.red, fontScale);
        SetTimeToNextEvent();
        GenerateEvents();
    }

    private void GenerateEvents() {
        events = new List<Event>();
        events.Add(
            new Event(
                "snow storm", 
                new string[] {"colder out", "better bundle up"},
                "villager", "fading warmth", "warmth", -1
            )
        );
        events.Add(
            new Event(
                "scarce resources",
                new string[] {"resources are scarce", "need to travel further"},
                "villager", "waning energy", "energy", -1
            )
        );
        events.Add(
            new Event(
                "ravenous mutant moths",
                new string[] {"a plague of moths", "no clothing is safe"},
                "villager", "rotting clothes", "clothing", -1
            )
        );
        events.Add(
            new Event(
                "acid rain",
                new string[] {"poison from the sky", "tools lose their edge"},
                "villager", "brittle tools", "tool", -1
            )
        );
        events.Add(
            new Event(
                "famine",
                new string[] {"a blight on the crops", "food production halted"},
                "production", "food", "food", -1
            )
        );
        events.Add(
            new Event(
                "monsoon",
                new string[] {"wood too wet to burn", "firewood production halted"},
                "production", "firewood", "firewood", -1
            )
        );
        events.Add(
            new Event(
                "cave-in",
                new string[] {"tragedy in the mines", "iron production halted"},
                "production", "iron", "iron", -1
            )
        );
        events.Add(
            new Event(
                "repairs in progress",
                new string[] {"the forges are cold", "tool production halted"},
                "production", "tool", "tool", -1
            )
        );
        events.Add(
            new Event(
                "forest fire",
                new string[] {"a violent firestorm", "wood production halted"},
                "production", "wood", "wood", -1
            )
        );
        events.Add(
            new Event(
                "warm and balmy",
                new string[] {"perfect weather", "villagers gain warmth"},
                "villager", "surging warmth", "warmth", 1
            )
        );
        if (currentEvent == null) {
            // set to avoid null pointers, but this first event will not be used
            SetCurrentEvent();
        }
    }

    private void UpdateTextBox() {
        if (xUnit != 0 && yUnit != 0 && xUnit == Utils.utils.xUnit && yUnit == Utils.utils.yUnit) {
            return;
        }
        xUnit = Utils.utils.xUnit;
        yUnit = Utils.utils.yUnit;
        textBox.Set(xUnit * 65, yUnit * 120, xUnit * 80, yUnit * 10);
        font.Update();
    }

    private void SetCurrentEvent() {
        currentEvent = events[Random.Range(0, events.Count)];
    }

    public void Manage() {
        UpdateTextBox();
        if (!State.state.IsActive()) {
            return;
        }
        if (currentEvent.active) {
            currentEvent.Update();
            return;
        } else {
            if (activated) {
                DeactivateEvent();
            }
        }
        if (!activated) {
            timeToNextEvent -= Time.deltaTime * SpeedSlider.slider.currentSliderValue;
            if (timeToNextEvent <= 0) {
                ActivateEvent();
            }
        }
    }

    private void ActivateEvent() {
        SetCurrentEvent();
        currentEvent.Trigger();
        activated = true;
    }

    public void DeactivateEvent() {
        SetTimeToNextEvent();
        Log.log.Add("conditions return to normal\n");
        lastEventEndTime = Utils.utils.currentGameTime;
        activated = false;
    }

    private void SetTimeToNextEvent() {
        timeToNextEvent = Random.Range(minTimeBetweenEvents, maxTimeBetweenEvents);
    }

    public void Draw() {
        if (currentEvent == null || !currentEvent.active) {
            return;
        }
        font.UpdateBad();
        GUI.Label(textBox, currentEvent.Repr(), font.font);
    }

    public int GetEventEffect(string testTarget, string testType) {
        if (currentEvent.active && currentEvent.target == testTarget && currentEvent.type == testType) {
            return currentEvent.potency;
        } else {
            return 0;
        }
    }

    public int GetEventEffect(List<string> testTargets, string testType) {
        int returnValue = 0;
        foreach (string entry in testTargets) {
            returnValue = GetEventEffect(entry, testType);
            if (returnValue != 0) {
               break;
            }
        }
        return returnValue;
    }

    public void Reset() {
        lastEventEndTime = 0f;
        timeToNextEvent = 0f;
        SetDefaults();
        // currentEvent = null;
        SetCurrentEvent();
    }

    public string GetEventsDebugRepr() {
        string theString = "EVENTS";
        theString += "\n    NEXT: " + timeToNextEvent;
        theString += "\n    CURRENT: " + currentEvent.DebugRepr() + "\n";
        foreach (Event entry in events) {
            theString += string.Format("\n{0:s}", entry.DebugRepr());
        }
        return theString;
    }
}
