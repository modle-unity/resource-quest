using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utils : MonoBehaviour {

    public static Utils utils;

    public float xUnit;
    public float yUnit;
    public Color textColor = Color.black;
    public Color textColorDim = new Color(0.75f, 0.75f, 0.75f);
    public Color textColorBad = Color.red;
    public Color textColorDebug = Color.blue;
    public Dictionary<string, GUIStyle> fonts = new Dictionary<string, GUIStyle>();
    static float thousand = 1000;
    static float million = 1000000;
    static float billion = 1000000000;
    static float trillion = 1000000000000;
    static string thousandLetter = "K";
    static string millionLetter = "M";
    static string billionLetter = "B";
    public float currentGameTime = 0f;
    public float loadedGameTime = 0f;
    public float lastSaveTime;

    void Awake() {
        // singleton pattern
        // why singleton?
        if (utils == null) {
            DontDestroyOnLoad(gameObject);
            utils = this;
        } else if (utils != this) {
            Destroy(gameObject);
        }
        SetDefaults();
    }

    void Update() {
        // adjust current game time by historic game time (savedGameTime) and current time since start (Time.time)
        currentGameTime = loadedGameTime + Time.time;
        SetUnits();
    }

    void SetDefaults() {
        SetUnits();
    }

    private void SetUnits() {
        xUnit = Screen.width * 0.005f;
        yUnit = Screen.height * 0.005f;
    }

    public int OscillateSprite(List<Texture2D> textures, int delayMod) {
        int input = (int)(Time.time * delayMod) % textures.Count;
        int length = textures.Count - 1;
        if (length == 0) {
            return 0;
        }

        return Mathf.Abs(((input + length) % (length * 2)) - length);
    }

    public string SquishNumber(float val) {
        float conversion = 1;
        string letter = "";

        if (val < thousand) {
            return string.Format("{0:0}{1:s}", (val / conversion), letter);
        }

        if (val > thousand) {
            conversion = thousand;
            letter = thousandLetter;
        }

        if (val > million) {
            conversion = million;
            letter = millionLetter;
        }

        if (val > billion) {
            conversion = billion;
            letter = billionLetter;
        }

        if (val > trillion) {
            return "just stop";
        }

        return string.Format("{0:0.0}{1:s}", (val / conversion), letter);
    }

    public void Reset() {
        currentGameTime = 0;
        loadedGameTime = 0;
        lastSaveTime = 0;
    }

    public string GetUtilsRepr() {
        string text = "Time since load: " + Time.time +
            "\nTotal game time: " + currentGameTime +
            "\nLast load time: " + loadedGameTime +
            "\nLast save time: " + lastSaveTime;
        return text;
    }
}
