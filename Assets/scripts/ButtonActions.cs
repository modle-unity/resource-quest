using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonActions {
  
    public Dictionary<string, System.Action> actions = new Dictionary<string, System.Action>();

    public ButtonActions() {
        actions["pause"] = Pause;
        actions["help"] = Help;
        actions["reload"] = Reload;
        actions["settings"] = Settings;
        actions["back"] = Pause;
        actions["blog"] = Blog;
        actions["gitlab"] = Gitlab;
        actions["confirmReset"] = ConfirmReset;
    }

    public void Pause() {
        State.state.ResetScreens();
        State.state.paused = !State.state.paused;
    }

    public void Help() {
        State.state.showHelp = !State.state.showHelp;
    }

    public void Settings() {
        State.state.ResetScreens();
        State.state.paused = true;
        State.state.showSettings = true;
        State.state.screen = "settings";
    }

    public void Reload() {
        ButtonHandler.reloadClicked = true;
    }

    public void ConfirmReset() {
        State.state.ResetScreens();
        State.state.restart = true;
    }

    public void Blog() {
        Application.OpenURL("https://games.matthewodle.com/");
    }

    public void Gitlab() {
        Application.OpenURL("https://gitlab.com/modle-unity/resource-quest-2");
    }
}
