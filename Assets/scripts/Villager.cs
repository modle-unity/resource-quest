using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Villager {
    public string assignment { get; set; }
    public Dictionary<string, Stat> stats = new Dictionary<string, Stat>();
    public Dictionary<string, int> statDelta = new Dictionary<string, int>();
    public List<string> conditions = new List<string>();
    private bool processed = false;
    private int deathTrigger = -50;
    public int assigned = 0;
    private int spriteWidthMultiplier = 9;
    private int spriteHeightMultiplier = 15;

    public List<Texture2D> spriteTextures = new List<Texture2D>();
    public Rect spriteRect;
    // defaultSpriteRect is used to track expected sprite parameters when villager is not being dragged/assigned
    public Rect defaultSpriteRect;
    private Vector2 currentDrag = new Vector2();
    public bool isDragged;
    private float xUnit = Utils.utils.xUnit;
    private float yUnit = Utils.utils.yUnit;
    public bool dead = false;
    private ConditionProcessor conditionProcessor;


    public Villager(List<Texture2D> _spriteTextures) {
        assignment = "";
        spriteTextures = _spriteTextures;
        SetSpriteParameters();
        SetStats();
        conditionProcessor = new ConditionProcessor(this);
    }

    private void SetSpriteParameters() {
        xUnit = Utils.utils.xUnit;
        yUnit = Utils.utils.yUnit;
        spriteRect.width = xUnit * spriteWidthMultiplier;
        spriteRect.height = yUnit * spriteHeightMultiplier;
        defaultSpriteRect.width = spriteRect.width;
        defaultSpriteRect.height = spriteRect.height;
    }

    private void SetStats() {
        int max = 100;
        int min = 0;
        this.stats["energy"] = new Stat("energy", min - 50, max, "food", "starving");
        this.stats["warmth"] = new Stat("warmth", min - 50, max, "firewood,flint", "freezing");
        this.stats["tool"] = new Stat("tool", min, max, "tool", "toolless");
        this.stats["clothing"] = new Stat("clothing", min, max, "clothing,pants", "naked");
        List<string> statKeys = stats.Keys.ToList();
        List<string> statDeltaKeys = statDelta.Keys.ToList();
        foreach (string name in statKeys) {
            if (!statDeltaKeys.Contains(name)) {
                statDelta.Add(name, 0);
            }
        }
    }

    public void Update(int index) {
        if (State.state.paused || dead) {
            return;
        }
        Reposition(index);
        ProcessStats();
        // resets conditions such as 'freezing' and 'starving'
        this.conditions.Clear();
        CheckStats();
    }

    private void ProcessStats() {
        assigned = (assignment == "" || dead ? 0 : 1);
        // processed flag prevents multiple executions in the same time division
        if (!IsEligibleForProcessing()) {
            return;
        }
        ResetStatDelta();
        conditionProcessor.Default();
        conditionProcessor.Assigned();
        conditionProcessor.Special();
        conditionProcessor.Event();
        ApplyStatDelta();
        this.processed = true;
    }

    private bool IsEligibleForProcessing() {
        if (State.state.CheckTick() || !VillagerHandler.handler.active) {
            this.processed = false;
            return false;
        }
        if (this.processed) {
            return false;
        }
        return true;
    }

    private void ResetStatDelta() {
        foreach (string entry in statDelta.Keys.ToList()) {
            statDelta[entry] = 0;
        }
    }

    private void ApplyStatDelta() {
        foreach (KeyValuePair<string, Stat> entry in stats) {
            this.stats[entry.Key].AdjustValue(statDelta[entry.Key]);
        }
    }

    private void CheckStats() {
        foreach (KeyValuePair<string, Stat> entry in stats) {
            Kill(entry.Value);
            entry.Value.Replenish();
            SetCondition(entry.Value);
        }
    }

    private void Kill(Stat stat) {
        if (!VillagerHandler.handler.vitalStats.Contains(stat.name)) {
            return;
        }
        if (stat.value <= deathTrigger) {
            this.dead = true;
            this.assignment = "dead";
            Log.log.Add(VillagerHandler.handler.deathMessages[stat.name]);
            BuildingHandler.handler.RemoveVillager(this.assignment);
        }
    }

    private void SetCondition(Stat stat) {
        // handles conditions such as 'freezing', 'starving', which is used in the condition display below buildings
        if (stat.value <= stat.conditionTrigger) {
            this.conditions.Add(stat.condition);
        }
    }

    public void Assign(string building) {
        this.assignment = building;
        if (!VillagerHandler.handler.active) {
            VillagerHandler.handler.active = true;
        }
    }

    public void Unassign() {
        Assign("");
    }

    public void Draw() {
        if (IsIdle()) {
            int textureKey = Utils.utils.OscillateSprite(spriteTextures, 6);
            GUI.DrawTexture(spriteRect, spriteTextures[textureKey]);
        }
    }

    private bool IsIdle() {
        return this.assignment == "";
    }

    public void Drag() {
        // disable if ui is otherwise occupied; if dragCount > 0, UI is occupied with this operation
        if (!GameControl.control.LockUIByType("villager")) {
            return;
        }

        // if villager isn't idle, ignore it
        if (!IsIdle()) {
            return;
        }

        Vector2 touchPosition = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

        // checks if sprite is within the drag square or the touch point is within sprite rect
        if (currentDrag.sqrMagnitude != 0 || spriteRect.Contains(touchPosition)) {
            // currentDrag is needed to handle selecting multiple villagers
            // without it, only one villager can be dragged at a time
            if (Input.GetMouseButton(0)) {
                // if mouse is down
                isDragged = true;
                currentDrag = touchPosition;
                spriteRect.x = touchPosition.x;
                spriteRect.y = touchPosition.y;
            } else {
                // when mouse is up
                isDragged = false;
                foreach (KeyValuePair<string, Building> entry in BuildingHandler.handler.buildings) {
                    if (entry.Value.buildingRects.label.Overlaps(spriteRect)) {
                        Assign(entry.Value.name);
                        break;
                    }
                }
                currentDrag.x = 0;
                currentDrag.y = 0;
                ResetPosition();
            }
        }
    }

    private void ResetPosition() {
        spriteRect.x = defaultSpriteRect.x;
        spriteRect.y = defaultSpriteRect.y;
    }

    private void Reposition(int index) {
        SetSpriteParameters();
        int offset = VillagerHandler.handler.posParams["offset"];
        int startX = VillagerHandler.handler.posParams["startX"];
        int startY = VillagerHandler.handler.posParams["startY"];
        int row = (int)Mathf.Floor(index / VillagerHandler.handler.spritesPerRow);
        defaultSpriteRect.x = startX + (index * offset) - (VillagerHandler.handler.spritesPerRow * offset * row);
        defaultSpriteRect.y = startY + (offset * row);
        ResetPosition();
    }

    public string DebugRepr() {
        return string.Format(
            "W:{0:d} | E:{1:d} | T:{2:d} | C:{3:d} | B:{4:s}",
            stats["warmth"].value,
            stats["energy"].value,
            stats["tool"].value,
            stats["clothing"].value,
            assignment
        );
    }

    private class ConditionProcessor {
        private int baseStatDelta = 2;
        private BuildingHandler handler = BuildingHandler.handler;
        private Villager villager;

        public ConditionProcessor(Villager _villager) {
            villager = _villager;
        }

        public void Default() {
            AdjustStat("clothing", -baseStatDelta);
            AdjustStat("energy", -baseStatDelta);
            AdjustStat("warmth", -(baseStatDelta * 3));
        }

        public void Assigned() {
            if (villager.assignment != "") {
                AdjustStat("energy", -baseStatDelta);
                AdjustStat("warmth", baseStatDelta);
                if (ToolUsed()) {
                    AdjustStat("tool", -baseStatDelta);
                }
            }
        }

        public void Special() {
            if (villager.stats["clothing"].value > 0) {
                AdjustStat("warmth", (baseStatDelta * 2));
            }
        }

        public void Event() {
            AdjustStat("clothing", EventHandler.handler.GetEventEffect("clothing", "villager"));
            AdjustStat("energy", EventHandler.handler.GetEventEffect("energy", "villager"));
            AdjustStat("warmth", EventHandler.handler.GetEventEffect("warmth", "villager"));
            if (ToolUsed()) {
                AdjustStat("tool", EventHandler.handler.GetEventEffect("tool", "villager"));
            }
        }

        private bool ToolUsed() {
            return villager.stats["tool"].value > 0
                && handler.buildings.Keys.Contains(villager.assignment)
                && handler.buildings[villager.assignment].IsProducing();
        }

        private void AdjustStat(string what, int amount) {
            villager.statDelta[what] += amount;
        }
    }
}
