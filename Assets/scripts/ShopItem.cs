using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class ShopItem {
    public string name;
    public string description;
    public string type;
    public string effect;
    public string target;
    public int cost;
    public Rect purchaseRect = Rect.zero;
    public Rect costRect = Rect.zero;
    public Rect coinRect = Rect.zero;
    public Rect iconRect = Rect.zero;
    public Rect labelRect = Rect.zero;

    public ShopItem(string _name, string _description, string _type, string _effect, string _target, int _cost) {
        name = _name;
        description = _description;
        type = _type;
        effect = _effect;
        target = _target;
        cost = _cost;
    }

    public void Trigger() {
        Log.log.Add(string.Format("{0:s}\n", string.Join("\n", description)));
    }

    public void UpdatePos(Rect textBox, float xUnit, float yUnit, int offsetMultiplier, int count) {
        purchaseRect.Set(
            textBox.x,
            textBox.y + offsetMultiplier * count,
            xUnit * 4,
            yUnit * 6
        );
        costRect.Set(
            textBox.x + xUnit * 5,
            textBox.y + offsetMultiplier * count,
            xUnit * 2,
            yUnit * 2
        );
        coinRect.Set(
            textBox.x + xUnit * 8,
            textBox.y + offsetMultiplier * count,
            xUnit * 2,
            yUnit * 2
        );
        iconRect.Set(
            textBox.x + xUnit * 12,
            textBox.y + offsetMultiplier * count,
            xUnit * 2,
            yUnit * 2
        );
        labelRect.Set(
            textBox.x + xUnit * 15,
            textBox.y + offsetMultiplier * count,
            textBox.width,
            offsetMultiplier
        );
    }

    public string DebugRepr() {
        return string.Format(
            "{0:d} | {1:s} | {2:s}",
            cost, name, effect
        );
    }
}
