using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Building {
    public string name;
    public string label;
    public string description;
    public float progress;
    public int villagers;
    public int villagersWithTools;
    private static float barMax;

    float baseProgressIncrement = 10f;
    float progressDelta;
    string progressDeltaRepr;

    int progressDivisor = 25;
    float villagerDiminishingReturns = 10;

    int addVillagerAmount = 10;
    int removeVillagerAmount = 10;

    float displayLastUpdated = 0;
    float timeBetweenDeltaTextUpdates = 0.1f;

    public Dictionary<string, int> consumes;
    public Dictionary<string, int> produces;
    public Dictionary<string, Rect> rects = new Dictionary<string, Rect>();
    public Dictionary<string, Texture2D> textures = new Dictionary<string, Texture2D>() {
        {"cancel", (Texture2D)Resources.Load("cancel")},
        {"border", (Texture2D)Resources.Load("theTexture3")},
        {"label", (Texture2D)Resources.Load("theTexture4")},
        {"minus", (Texture2D)Resources.Load("minus")},
        {"plus", (Texture2D)Resources.Load("plus")},
        {"progress", (Texture2D)Resources.Load("theTexture5")},
        {"tool", (Texture2D)Resources.Load("stores/tool")},
        {"villager", (Texture2D)Resources.Load("villagers/villager0-0")},
        {"addVillager", (Texture2D)Resources.Load("arrow-up")},
        {"removeVillager", (Texture2D)Resources.Load("arrow-down")},
        {"times1", (Texture2D)Resources.Load("times1")},
        {"times10", (Texture2D)Resources.Load("times10")},
        {"times100", (Texture2D)Resources.Load("times100")}
    };

    private float xUnit = Utils.utils.xUnit;
    private float yUnit = Utils.utils.yUnit;
    public CustomFont font;
    private int fontScale = 3;

    private bool eligible = false;
    private bool active = false;

    public BuildingImageRects buildingRects;

    List<int> assignmentMultiplierValues = new List<int>(new int[] {1, 10, 100});
    int assignmentMultiplierIndex = 0;
    int assignmentMultiplier = 1;
    Texture2D assignmentMultiplierTexture;

    public Building(string _name, string _label, string _description,
            Dictionary<string, int> _consumes, Dictionary<string, int> _produces,
            int _yOffsetMultiplier, Dictionary<string, int> posParams) {
        name = _name;
        label = _label;
        description = _description;
        consumes = _consumes;
        produces = _produces;
        progress = 0;
        villagers = 0;
        villagersWithTools = 0;
        font = new CustomFont(Utils.utils.textColor, fontScale);
        barMax = posParams["theMod"];
        buildingRects = new BuildingImageRects(_yOffsetMultiplier);
        assignmentMultiplier = assignmentMultiplierValues[assignmentMultiplierIndex];
        assignmentMultiplierTexture = textures["times" + assignmentMultiplier];
        UpdatePosition(posParams);
    }

    public void UpdatePosition(Dictionary<string, int> posParams) {
        if (buildingRects.progress.x != 0 && xUnit == Utils.utils.xUnit && yUnit == Utils.utils.yUnit) {
            return;
        }
        xUnit = Utils.utils.xUnit;
        yUnit = Utils.utils.yUnit;
        UpdateRects(posParams);
    }

    private void UpdateRects(Dictionary<string, int> posParams) {
        barMax = posParams["theMod"];

        buildingRects.Update(xUnit, yUnit, posParams);
    }

    public void ManageProductionState() {
        CheckProductionEligibility();
        if (!this.active && this.eligible) {
            HandleConsumption();
        }
        SetProgressDeltaText();
        if (IsProducing()) {
            UpdateProgress();
            ProcessProductionCompletion();
        }
    }

    private void CheckProductionEligibility() {
        this.eligible = true;
        foreach (KeyValuePair<string, int> entry in consumes) {
            this.eligible = this.eligible && StoreHandler.handler.values[entry.Key] >= entry.Value;
        }
    }

    private void HandleConsumption() {
        foreach (KeyValuePair<string, int> entry in consumes) {
            StoreHandler.handler.stores[entry.Key].Consume(entry.Value);
        }
        this.active = true;
    }

    private void SetProgressDeltaText() {
        // prevents jittering in the value
        if ((Time.time - displayLastUpdated) > timeBetweenDeltaTextUpdates) {
            float progressPercent = buildingRects.progress.width / barMax * 100;
            // progressDeltaRepr = "x" + (int)(progressDelta + 1) + "\n" + (int)progressPercent + "%";
            // | needs to be in single quotes because we need a char, not a string
            string deltaGuage = new string('|', (int)(progressDelta + 1));
            progressDeltaRepr = deltaGuage + "\n" + (int)progressPercent + "%";
            displayLastUpdated = Time.time;
        }
    }

    public bool IsProducing() {
        return !IsBlockedByEvent() && this.active;
    }

    private bool IsBlockedByEvent() {
        return EventHandler.handler.GetEventEffect(produces.Keys.ToList(), "production") != 0;
    }

    public void UpdateProgress() {
        SetProgressDelta();
        this.progress += progressDelta;
        this.buildingRects.progress.width = this.progress % barMax;
    }

    private void SetProgressDelta() {
        // this can be tweaked to adjust difficulty
        // divide by lower number = easier
        // difficulty setting in-game?
        int toollessVillagers = villagers - villagersWithTools;
        float toolScaledVillagers = this.villagersWithTools * State.state.toolPower;
        float gameParamAdjustedIncrement = baseProgressIncrement * xUnit * Time.deltaTime * SpeedSlider.slider.currentSliderValue;

        // villagers with and without tools need to be additive
        // or a possibility of an exponential outcome can occur when villagers suddenly become toolless
        progressDelta = gameParamAdjustedIncrement * (toollessVillagers + toolScaledVillagers) / progressDivisor;

        // diminishing returns: ((x+1)**(1-n) - 1)/(1-n)
        // x = increment; n = diminishing returns value
        // progressDelta = (Mathf.Pow(toolAdjustedIncrement + 1, 1 - villagerDiminishingReturns) - 1) / (1 - villagerDiminishingReturns);
    }

    private void ProcessProductionCompletion() {
        if (this.progress > barMax) {
            // if (this.eligible) {
                // this.progress -= barMax;
            // } else {
            //     this.progress = 0f;
            // }
            this.progress = 0f;
            this.active = false;
            foreach (KeyValuePair<string, int> entry in produces) {
                Score.score.Add(1);
                StoreHandler.handler.stores[entry.Key].Add(entry.Value);
            }
        }
    }

    public void Draw() {
        font.Update();
        GUI.DrawTexture(this.buildingRects.border, this.textures["border"]);
        GUI.DrawTexture(this.buildingRects.multiplier, this.textures["label"]);
        GUI.DrawTexture(this.buildingRects.progress, this.textures["progress"]);
        // this is actually the inner rect, not a label, just using the same dimensions
        GUI.DrawTexture(this.buildingRects.label, this.textures["label"]);
        GUI.DrawTexture(this.buildingRects.progress, this.textures["progress"]);
        GUI.Label(this.buildingRects.label, GetText(), this.font.font);
        GUI.Label(this.buildingRects.multiplier, progressDeltaRepr, this.font.font);
        GUI.DrawTexture(this.buildingRects.villager, this.textures["villager"]);
        GUI.Label(this.buildingRects.villagerLabel, GetVillagerText(), this.font.font);
        GUI.DrawTexture(this.buildingRects.villagersWithTools, this.textures["tool"]);
        GUI.Label(this.buildingRects.villagersWithToolsLabel, this.villagersWithTools.ToString(), this.font.font);

        // anything that shouldn't be drawn when the help screen is open should go here
        if (!State.state.showHelp) {
            GUI.DrawTexture(this.buildingRects.plus, this.textures["plus"]);
            DrawIcons(produces, "plus");
            if (this.consumes.Keys.Count > 0) {
                GUI.DrawTexture(this.buildingRects.minus, this.textures["minus"]);
                DrawIcons(consumes, "minus");
            }

            // assignment handlers
            if (GUI.Button(this.buildingRects.assignmentMultiplier, assignmentMultiplierTexture)) {
                CycleAssignmentMultiplier();
            }
            if (GUI.Button(this.buildingRects.cancel, this.textures["cancel"])) {
                ClearAssignments();
                VillagerHandler.handler.SortVillagers();
            }
            if (GUI.Button(this.buildingRects.addVillager, this.textures["addVillager"])) {
                VillagerHandler.handler.AssignVillagers(name, this.assignmentMultiplier);
                Debug.Log("that's an add click");
            }
            if (GUI.Button(this.buildingRects.removeVillager, this.textures["removeVillager"])) {
                Debug.Log("that's a remove click");
                VillagerHandler.handler.UnassignVillagers(name, this.assignmentMultiplier);
            }
        }
    }

    private void CycleAssignmentMultiplier() {
        // will set current multipler index and texture
        // will never increase beyond length of values array
        this.assignmentMultiplierIndex += 1;
        if (this.assignmentMultiplierIndex == this.assignmentMultiplierValues.Count) {
            this.assignmentMultiplierIndex = 0;
        }
        this.assignmentMultiplier = this.assignmentMultiplierValues[this.assignmentMultiplierIndex];
        string textureName = "times" + this.assignmentMultiplier;
        if (this.textures.ContainsKey(textureName)) {
            this.assignmentMultiplierTexture = this.textures[textureName];
        } else {
            Debug.Log("multiplier texture " + textureName + " not found in textures dictionary");
        }
    }

    private string GetText() {
        return this.label;
    }

    private void DrawIcons(Dictionary<string, int> iconRects, string iconType) {
        float offset = this.buildingRects.progressOffset * 0.75f;
        float labelOffset = offset / 3f;
        int iconCount = 0;
        foreach (KeyValuePair<string, int> entry in iconRects) {
            Rect newRect = this.buildingRects.icon;
            newRect.x = newRect.x + (iconCount * offset);
            if (iconType == "plus") {
                newRect.y = this.buildingRects.plus.y;
            } else if (iconType == "minus") {
                newRect.y = this.buildingRects.minus.y;
            }

            // show the icon
            GUI.DrawTexture(newRect, StoreHandler.handler.stores[entry.Key].spriteTexture);
            newRect.x = newRect.x + labelOffset;
            // show the quantity next to the icon
            GUI.Label(newRect, "x" + entry.Value.ToString(), this.font.font);
            iconCount++;
        }
    }

    private void ClearAssignments() {
        VillagerHandler.handler.UnassignVillagers(name, villagers);
    }

    public string DebugRepr() {
        return string.Format(
            "V:{0:d} | T:{1:d} | {2:s} | deltaP:{3:f} | P:{4:f}",
            villagers, villagersWithTools, name, progressDelta, this.progress / barMax
        );
    }

    private string GetVillagerText() {
        return Utils.utils.SquishNumber((float)this.villagers);
    }

    public class BuildingImageRects {
        // creating a class instead of using a map due to inability to update parameters of rect while part of the map
        // without recreating the rects, which was causing too many Rect objects and stutter with garbage collection
        int yOffsetMultiplier = 0;
        public int progressOffset = 0;
        Rect baseIconRect = Rect.zero;
        public Rect progress = Rect.zero;
        public Rect label = Rect.zero;
        public Rect villager = Rect.zero;
        public Rect villagerLabel = Rect.zero;
        public Rect villagersWithTools = Rect.zero;
        public Rect villagersWithToolsLabel = Rect.zero;
        public Rect border = Rect.zero;
        public Rect cancel = Rect.zero;
        public Rect plus = Rect.zero;
        public Rect minus = Rect.zero;
        public Rect icon = Rect.zero;
        public Rect multiplier = Rect.zero;
        public Rect assignmentMultiplier = Rect.zero;
        public Rect addVillager = Rect.zero;
        public Rect removeVillager = Rect.zero;

        public BuildingImageRects(int _yOffsetMultiplier) {
            yOffsetMultiplier = _yOffsetMultiplier;
        }

        public void Update(float xUnit, float yUnit, Dictionary<string, int> posParams) {
            progressOffset = posParams["progressOffset"];
            progress.Set(
                posParams["progressX"],
                posParams["progressY"] + yOffsetMultiplier * progressOffset,
                0,
                posParams["progressHeight"]
            );

            multiplier.Set(
                posParams["labelX"] - xUnit * 6,
                progress.y - xUnit,
                xUnit * 2f,
                xUnit * 2f
            );

            // layout and identifiers
            baseIconRect.Set(
                posParams["labelX"],
                progress.y - xUnit,
                xUnit * 2f,
                xUnit * 2f
            );
            border.Set(
                posParams["labelX"] - xUnit,
                progress.y - xUnit,
                posParams["progressWidth"] + xUnit * 2f,
                posParams["progressHeight"] + xUnit * 2f
            );
            label.Set(
                posParams["labelX"],
                progress.y,
                posParams["progressWidth"],
                posParams["progressHeight"]
            );

            // assignment indicators
            villager.Set(
                posParams["labelX"],
                progress.y + yUnit * 3,
                baseIconRect.width * 2,
                baseIconRect.height * 2
            );
            villagerLabel.Set(
                posParams["labelX"] + xUnit * 4,
                progress.y + yUnit * 5,
                baseIconRect.width,
                baseIconRect.height
            );
            villagersWithTools.Set(
                posParams["labelX"] + xUnit * 8,
                progress.y + yUnit * 6,
                baseIconRect.width * 0.5f,
                baseIconRect.height * 0.5f
            );
            villagersWithToolsLabel.Set(
                posParams["labelX"] + xUnit * 10,
                progress.y + yUnit * 5,
                baseIconRect.width,
                baseIconRect.height
            );

            // villager assignment controls
            float assignmentButtonMult = 2.75f;
            assignmentMultiplier.Set(
                posParams["labelX"] + xUnit * 15.0f,
                progress.y,
                baseIconRect.width * assignmentButtonMult,
                baseIconRect.height * assignmentButtonMult
            );
            addVillager.Set(
                posParams["labelX"] + xUnit * 22.5f,
                progress.y,
                baseIconRect.width * assignmentButtonMult,
                baseIconRect.height * assignmentButtonMult
            );
            removeVillager.Set(
                posParams["labelX"] + xUnit * 30.0f,
                progress.y,
                baseIconRect.width * assignmentButtonMult,
                baseIconRect.height * assignmentButtonMult
            );
            cancel.Set(
                posParams["labelX"] + xUnit * 37.5f,
                progress.y,
                baseIconRect.width * assignmentButtonMult,
                baseIconRect.height * assignmentButtonMult
            );

            // consumes/produces icons
            plus.Set(
                posParams["labelX"] + xUnit * 60f,
                baseIconRect.y + yUnit * 2.0f,
                baseIconRect.width,
                baseIconRect.height
            );
            minus.Set(
                posParams["labelX"] + xUnit * 60f,
                baseIconRect.y + yUnit * 8.0f,
                baseIconRect.width,
                baseIconRect.height
            );
            icon.Set(
                posParams["labelX"] + xUnit * 63f,
                plus.y,
                baseIconRect.width,
                baseIconRect.height
            );
        }
    }
}
