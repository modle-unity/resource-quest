using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VillagerHandler : MonoBehaviour {

    public static VillagerHandler handler;

    public List<Villager> villagers;
    public List<Dictionary<string, Stat>> loadedStats = new List<Dictionary<string, Stat>>();
    public List<string> loadedAssignments = new List<string>();
    public Dictionary<string, int> posParams = new Dictionary<string, int>();
    private float xUnit;
    private float yUnit;
    public int spritesPerRow;
    private int numAssignedAtLastDraw = 0;
    private int numAssigned = 0;
    private int minTimeBetweenNew = 20;
    private int maxTimeBetweenNew = 40;
    public float timeToNext = 0;
    public float timeOfLastRover = 0;
    public bool active;
    int defaultCount = 20;
    public int count = 0;
    public int deadCount = 0;
    public int dragCount = 0;
    public List<string> vitalStats = new List<string>(new string[] {"energy", "warmth"});
    public Dictionary<string, string> deathMessages = new Dictionary<string, string>() {
        {"warmth", "a villager has frozen to death"},
        {"energy", "a villager has starved to death"}
    };

    Rect textBox;
    CustomFont font;
    int fontScale = 2;

    void Awake() {
        // singleton pattern
        if (handler == null) {
            DontDestroyOnLoad(gameObject);
            handler = this;
        } else if (handler != this) {
            Destroy(gameObject);
        }
    }

    public void SetDefaults() {
        spritesPerRow = 4;
        SetVillagerCount();
        SetPositions();
        GenerateVillagers();
        SetTimeToNextVillager();
        ApplyLoadedStats();
        ApplyLoadedAssignments();
        SetupMouseCounterTextBox();
    }

    private void SetPositions() {
        if (xUnit != 0 && yUnit != 0 && xUnit == Utils.utils.xUnit && yUnit == Utils.utils.yUnit) {
            return;
        }
        xUnit = Utils.utils.xUnit;
        yUnit = Utils.utils.yUnit;
        SetDefaultPositionParams();
    }

    private void SetDefaultPositionParams() {
        posParams["offset"] = (int)(xUnit * 10);
        posParams["startX"] = (int)(xUnit * 10);
        posParams["startY"] = (int)(yUnit * 130);
    }

    public void SetVillagerCount() {
        count = (count == 0 && !State.state.gameOver ? defaultCount : count);
    }

    private void GenerateVillagers() {
        villagers = new List<Villager>();
        while (villagers.Count < count) {
            villagers.Add(CreateVillager());
        }
    }

    private Villager CreateVillager() {
        int theCount = villagers.Count;
        int row = (int)Mathf.Floor(theCount / spritesPerRow);

        List<Texture2D> spriteTextures = LoadSpriteList();

        return new Villager(spriteTextures);
    }

    public List<Texture2D> LoadSpriteList() {
        // load random sprite list
        int spriteNum = Random.Range(0, 5); // there are only 5 sprites
        List<Texture2D> spriteTextures = new List<Texture2D>();
        while (spriteTextures.Count < 6) {
            spriteTextures.Add((Texture2D)Resources.Load(string.Format("villagers/villager{0}-{1}", spriteTextures.Count, spriteNum)));
        }
        return spriteTextures;
    }

    private void SetTimeToNextVillager() {
        timeToNext = Random.Range(minTimeBetweenNew, maxTimeBetweenNew);
    }

    private void ApplyLoadedStats() {
        if (loadedStats.Count == 0) {
            return;
        }
        int index = 0;
        foreach(Dictionary<string, Stat> entry in loadedStats) {
            if (villagers.Count >= index + 1) {
                villagers[index].stats = entry;
                index++;
            } else {
                break;
            }
        }
    }

    private void ApplyLoadedAssignments() {
        if (loadedAssignments.Count == 0) {
            return;
        }
        int index = 0;
        foreach(string entry in loadedAssignments) {
            if (villagers.Count >= index + 1) {
                villagers[index].assignment = entry;
                index++;
            } else {
                break;
            }
        }
    }

    public void Manage() {
        SetPositions();
        int index = 0;
        // why prevent during mouse down?
        if (!State.state.MouseDown()) {
            // remove all villagers that are dead, increment the total count
            deadCount += villagers.RemoveAll(entry => entry.dead);
        }
        foreach (Villager entry in villagers) {
            entry.Update(index);
            index++;
        }
        timeToNext -= Time.deltaTime * SpeedSlider.slider.currentSliderValue;
        if (timeToNext < 0 && State.state.IsActive()) {
            villagers.Add(CreateVillager());
            if (CanBeSorted()) {
                SortVillagers();
            }
            timeOfLastRover = Utils.utils.currentGameTime;
            SetTimeToNextVillager();
            Log.log.Add("a rover joins your band\n");
        }
        CountDraggedVillagers();
        CheckUILockState();
    }

    public int CountBuildingVillagers(string buildingName) {
        int theCount = 0;
        foreach (Villager entry in villagers) {
            if (entry.assignment == buildingName) {
                theCount++;
            }
        }
        return theCount;
    }

    public int CountVillagersWithTools(string buildingName) {
        int villagersWithTools = 0;
        foreach (Villager entry in villagers) {
            if (entry.assignment == buildingName) {
                villagersWithTools += (entry.stats["tool"].value > 0 ? 1 : 0);
            }
        }
        return villagersWithTools;
    }

    public void CountDraggedVillagers() {
        int count = 0;
        foreach (Villager entry in villagers) {
            if (entry.isDragged) {
                count += 1;
            }
        }
        dragCount = count;
    }

    public void CheckUILockState() {
        if (dragCount > 0) {
            return;
        }
        GameControl.control.UnlockUI("villager");
    }

    public void Draw() {
        numAssigned = 0;
        foreach (Villager entry in villagers) {
            ProcessVillager(entry);
        }
        if (numAssignedAtLastDraw != numAssigned) {
            SortVillagers();
        }
        numAssignedAtLastDraw = numAssigned;
        DrawMouseCounter();
    }

    private void ProcessVillager(Villager villager) {
        villager.Draw();
        villager.Drag();
        numAssigned += villager.assigned;
    }

    public void SortVillagers() {
        villagers.Sort(delegate(Villager x, Villager y) {
            return x.assignment.CompareTo(y.assignment);
        });
    }

    private bool CanBeSorted() {
        bool sortable = !State.state.MouseDown();
        return sortable;
    }

    public void AssignVillagers(string buildingName, int amount) {
        HandleVillagerAdjustment(buildingName, amount, "add");
    }

    public void UnassignVillagers(string buildingName, int amount) {
        HandleVillagerAdjustment(buildingName, amount, "remove");
    }

    private void HandleVillagerAdjustment(string buildingName, int amount, string direction) {
        int adjusted = 0;
        // may be able to use some sort of List filter method instead
        foreach (Villager villager in villagers) {
            if (adjusted >= amount) {
                return;
            }
            if (direction == "add") {
                if (villager.assignment == "") {
                    villager.Assign(buildingName);
                    adjusted++;
                }
            } else if (direction == "remove") {
                if (villager.assignment == buildingName) {
                    villager.Unassign();
                    adjusted++;
                }
            } else {
                return;
            }
        }
    }

    public bool AllDead() {
        return villagers.Count == 0 ? true : false;
    }

    public string GetVillagersDebugRepr() {
        string theString = "VILLAGERS: " + villagers.Count + " / UNASSIGNED: " + (villagers.Count - numAssignedAtLastDraw);
        theString += "\nW: Warmth, E: Energy, T: Tool, C: Clothing, B: Building";
        int index = 0;
        foreach (Villager entry in villagers) {
            if (index > 60) {
                break;
            }
            theString += string.Format("\n{0:s}", entry.DebugRepr());
            index++;
        }
        return theString;
    }

    // is there a way to use filtering here?
    public List<Dictionary<string, Stat>> GetVillagerStats() {
        List<Dictionary<string, Stat>> theStats = new List<Dictionary<string, Stat>>();
        foreach (Villager entry in villagers) {
            theStats.Add(entry.stats);
        }
        return theStats;
    }

    // is there a way to use filtering here?
    public List<string> GetVillagerAssignments() {
        List<string> theAssignments = new List<string>();
        foreach (Villager entry in villagers) {
            theAssignments.Add(entry.assignment);
        }
        return theAssignments;
    }

    public void Load() {
        SetDefaults();
    }

    public void Reset() {
        villagers = new List<Villager>();
        count = defaultCount;
        deadCount = 0;
        timeOfLastRover = 0;
        active = false;
        loadedStats = new List<Dictionary<string, Stat>>();
        loadedAssignments = new List<string>();
        SetDefaults();
    }

    void SetupMouseCounterTextBox() {
        font = new CustomFont(Color.black, fontScale);
        textBox = new Rect(0, 0, 0, 0);
    }

    void DrawMouseCounter() {
        if (dragCount > 0) {
            Debug.Log("That's a drag count" + dragCount.ToString());
            textBox.x = Input.mousePosition.x + xUnit * 5;
            textBox.y = Screen.height - Input.mousePosition.y;
            GUI.Label(textBox, dragCount.ToString(), font.font);
        }
    }
}
