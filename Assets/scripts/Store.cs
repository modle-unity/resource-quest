using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Store {

    public string name;
    public string labelValue;
    public int row;
    public int offsetMultiplier;

    public Rect labelRect;
    private Rect spriteRect;
    public List<Rect> rectsToDraw = new List<Rect>();

    public Texture2D spriteTexture;
    public Dictionary<string, CustomFont> fonts = new Dictionary<string, CustomFont>();
    public Dictionary<string, int> posParams;

    private int lastSpawnedIconIndex;
    private List<AnimatedStore> spawnIcons = new List<AnimatedStore>();
    private int maxIcons = 100;
    private int fontScale = 3;

    private float xUnit = Utils.utils.xUnit;
    private float yUnit = Utils.utils.yUnit;

    public Store(string _name, string _labelValue, int _row, int _offsetMultiplier, Dictionary<string, int> posParams, Dictionary<string, Texture2D> _textures) {
        this.posParams = posParams;
        this.name = _name;
        this.labelValue = _labelValue;
        this.row = _row;
        this.offsetMultiplier = _offsetMultiplier;
        SetRects();
        this.spriteTexture = _textures[this.name];
        this.fonts["good"] = new CustomFont(Color.black, fontScale);
        this.fonts["bad"] = new CustomFont(Color.red, fontScale);
        while (this.spawnIcons.Count < this.maxIcons) {
            this.spawnIcons.Add(new AnimatedStore(
                this.spriteRect,
                this.spriteTexture,
                this.fonts
            ));
        }
    }

    private void SetRects() {
        UpdateLabelRect();
        UpdateSpriteRect();
        UpdatedAnimatedLabels();
    }

    private void UpdateLabelRect() {
        labelRect.Set(
            GetXPosition() + this.xUnit * 7,
            GetYPosition(),
            posParams["width"],
            posParams["height"]
        );
    }

    private void UpdateSpriteRect() {
        spriteRect.Set(
            GetXPosition(),
            GetYPosition(),
            posParams["width"],
            posParams["height"]
        );
    }

    private void UpdatedAnimatedLabels() {
        float newAnimatedXPos = GetXPosition();
        float newAnimatedYPos = GetYPosition();
        foreach (AnimatedStore entry in spawnIcons) {
            entry.Update(newAnimatedXPos, newAnimatedYPos);
        }
    }

    private float GetXPosition() {
        return posParams["x"] + this.offsetMultiplier * posParams["xOffset"];
    }

    private float GetYPosition() {
        return posParams["y"] + this.row * posParams["yOffset"];
    }

    public void Add(int _quantity) {
        SpawnAddIcon(_quantity);
        StoreHandler.handler.values[name] += _quantity;
        StoreHandler.handler.produced[name] += _quantity;
    }

    void SpawnAddIcon(int quantity) {
        this.spawnIcons[this.lastSpawnedIconIndex].Activate(quantity);
        this.lastSpawnedIconIndex = (this.lastSpawnedIconIndex + 1) % this.spawnIcons.Count;
    }

    public bool Consume(int _quantity) {
        if (StoreHandler.handler.values[name] > 0) {
            SpawnAddIcon(-_quantity);
            StoreHandler.handler.values[name] -= _quantity;
            StoreHandler.handler.consumed[name] += _quantity;
            return true;
        }
        return false;
    }

    public void Update(Dictionary<string, int> _posParams) {
        posParams = _posParams;
        UpdatePositions();
    }

    public void Draw() {
        UpdateFonts();
        if (!State.state.showHelp) {
            GUI.Label(this.labelRect, GetText(), GetStyle());
            GUI.DrawTexture(this.spriteRect, this.spriteTexture);
        }
        foreach (AnimatedStore entry in this.spawnIcons) {
            entry.Manage();
        }
    }

    private void UpdatePositions() {
        if (xUnit != 0 && yUnit != 0 && xUnit == Utils.utils.xUnit && yUnit == Utils.utils.yUnit) {
            return;
        }
        xUnit = Utils.utils.xUnit;
        yUnit = Utils.utils.yUnit;
        SetRects();
    }

    private void UpdateFonts() {
        this.fonts["good"].Update();
        this.fonts["bad"].UpdateBad();
    }

    string GetText() {
        return Utils.utils.SquishNumber(StoreHandler.handler.values[name]);
    }

    GUIStyle GetStyle() {
        return StoreHandler.handler.values[name] > 0 ? fonts["good"].font : fonts["bad"].font;
    }
}
