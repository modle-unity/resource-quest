using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Log : MonoBehaviour {

    public static Log log;

    private int maxEntryCount = 15;
    public string text { get; set; }
    public Rect textBox = Rect.zero;
    public Rect textBoxOutline = Rect.zero;
    public Texture2D textBoxOutlineTexture;
    public List<string> entries = new List<string>();
    public CustomFont font;
    private float fontScale = 3.5f;
    private float xUnit;
    private float yUnit;

    void Awake() {
        // singleton pattern
        if (log == null) {
            DontDestroyOnLoad(gameObject);
            log = this;
        } else if (log != this) {
            Destroy(gameObject);
        }
        SetDefaults();
    }

    private void SetDefaults() {
        textBoxOutlineTexture = (Texture2D)Resources.Load("theTexture6");
        font = new CustomFont(Color.black, fontScale);
        AddStartingMessages();
        UpdateDimensions();
    }

    private void AddStartingMessages() {
        Add("=================================");
        Add("");
        Add("Good luck!");
        Add("Press the ? (help) button for stats.");
        Add("to stay alive.");
        Add("to produce the resources they need");
        Add("Assign villagers to buildings");
        Add("");
        Add("=================================");
        Add("");
        Add("Welcome to Resource Quest!");
        Add("");
        Add("=================================");
        Add("");
        Add("");
        Add("");
    }

    public void Add(string entry) {
        entries.Insert(0, entry.ToString());
    }

    public void Update() {
        UpdateDimensions();
        Cleanup();
        if (entries.Count > 0) {
            SetText();
        }
    }

    private void UpdateDimensions() {
        if (xUnit != 0 && yUnit != 0 && xUnit == Utils.utils.xUnit && yUnit == Utils.utils.yUnit) {
            return;
        }
        xUnit = Utils.utils.xUnit;
        yUnit = Utils.utils.yUnit;
        textBox.Set(
            xUnit * 100,
            yUnit * 105,
            xUnit * 10,
            yUnit * 10
        );
        textBoxOutline.Set(
            textBox.x - xUnit,
            textBox.y - yUnit,
            xUnit * 40,
            Screen.height
        );
        font.Update();
    }

    private void Cleanup() {
        while (entries.Count > maxEntryCount) {
            entries.RemoveAt(entries.Count - 1);
        }
    }

    private void SetText() {
        string theText = "";
        foreach (string entry in entries) {
            theText += entry + "\n";
        }
        text = theText;
    }

    public void Draw() {
        font.Update();
        GUI.DrawTexture(textBoxOutline, textBoxOutlineTexture);
        GUI.Label(textBox, text, font.font);
    }

    public void Reset() {
        SetDefaults();
    }
}
