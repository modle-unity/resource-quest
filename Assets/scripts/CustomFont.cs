using UnityEngine;

public class CustomFont {
    public GUIStyle font;

    Color defaultColor;

    float xUnit;
    float yUnit;

    float fontScale;

    public CustomFont(Color color, float _fontScale) {
        font = new GUIStyle();
        defaultColor = color;
        font.normal.textColor = color;
        this.fontScale = _fontScale;
        font.fontSize = 0;
    }

    public void Update() {
        UpdateSize();
        font.normal.textColor = (State.state.showHelp || State.state.paused) ? Utils.utils.textColorDim : defaultColor;
    }

    private void UpdateSize() {
        SetPositionValues();
        font.fontSize = (int)(yUnit * fontScale);
    }

    public void SetPositionValues() {
        if (xUnit != 0 && yUnit != 0 && xUnit == Utils.utils.xUnit && yUnit == Utils.utils.yUnit) {
            return;
        }
        xUnit = Utils.utils.xUnit;
        yUnit = Utils.utils.yUnit;
    }

    public void UpdateBad() {
        UpdateSize();
        font.normal.textColor = (State.state.showHelp || State.state.paused) ? Utils.utils.textColorDim : Utils.utils.textColorBad;
    }
}
