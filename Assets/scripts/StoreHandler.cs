using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StoreHandler : MonoBehaviour {

    public static StoreHandler handler;

    public Dictionary<string, Store> stores = new Dictionary<string, Store>();
    public Dictionary<string, Texture2D> textures = new Dictionary<string, Texture2D>();
    public Dictionary<string, float> values = new Dictionary<string, float>();
    public Dictionary<string, int> consumed = new Dictionary<string, int>();
    public Dictionary<string, int> produced = new Dictionary<string, int>();
    List<string> storeNames = new List<string>() {
        "food", "firewood", "wood", "iron", "hide", "tool", "clothing", "flint", "pants", "bow"
    };

    private Dictionary<string, int> posParams = new Dictionary<string, int>();
    private float xUnit;
    private float yUnit;

    Rect outline = Rect.zero;
    Texture2D outlineTexture;

    void Awake() {
        // singleton pattern
        if (handler == null) {
            DontDestroyOnLoad(gameObject);
            handler = this;
        } else if (handler != this) {
            Destroy(gameObject);
        }
    }

    public void Update() {
        SetPositionValues();
    }

    public void SetDefaults() {
        SetPositionValues();
        LoadTextures();
        GenerateStores();
    }

    private void SetPositionValues() {
        if (xUnit != 0 && yUnit != 0 && xUnit == Utils.utils.xUnit && yUnit == Utils.utils.yUnit) {
            return;
        }
        xUnit = Utils.utils.xUnit;
        yUnit = Utils.utils.yUnit;
        SetDefaultPositionParams();
        SetOutlineParams();
        UpdateStores();
    }

    private void LoadTextures() {
        foreach(string name in storeNames) {
            textures[name] = (Texture2D)Resources.Load("stores/" + name);
        }
    }

    private void SetDefaultPositionParams() {
        posParams["x"] = (int)(xUnit * 100);
        posParams["y"] = (int)(yUnit * 20);
        posParams["xOffset"] = (int)(xUnit * 18);
        posParams["yOffset"] = (int)(yUnit * 10);
        posParams["width"] = (int)(xUnit * 3);
        posParams["height"] = (int)(yUnit * 5);
    }

    private void SetOutlineParams() {
        outline.Set(
            posParams["x"] - xUnit,
            posParams["y"] - yUnit,
            posParams["xOffset"] * 5,
            posParams["yOffset"] * 2
        );
        outlineTexture = (Texture2D)Resources.Load("theTexture6");
    }

    private void UpdateStores() {
        foreach (KeyValuePair<string, Store> entry in stores) {
            entry.Value.Update(posParams);
        }
    }

    private void GenerateStores() {
        int offsetMultiplier = 0;
        int row = 0;

        // Store signature: Store(string _name, string _labelValue, int _row, int _offsetMultiplier, Dictionary<string, int> posParams, Dictionary<string, Texture2D> _textures)
        // first row: row + 0
        // a reference to posParams will be applied to each Store; posParams updates when screen units change 
        stores["food"] = new Store(
            "food", "energy plus",
            row, offsetMultiplier, posParams, textures
        );
        stores["wood"] = new Store(
            "wood", "firewood and tools",
            row, offsetMultiplier + 1, posParams, textures
        );
        stores["iron"] = new Store(
            "iron", "tools",
            row, offsetMultiplier + 2, posParams, textures
        );
        stores["hide"] = new Store(
            "hide", "clothing",
            row, offsetMultiplier + 3, posParams, textures
        );
        stores["bow"] = new Store(
            "bow", "twang!",
            row, offsetMultiplier + 4, posParams, textures
        );
        
        // second row: row + 1
        stores["firewood"] = new Store(
            "firewood", "warmth plus",
            row + 1, offsetMultiplier, posParams, textures
        );
        stores["tool"] = new Store(
            "tool", "productivity plus",
            row + 1, offsetMultiplier + 1, posParams, textures
        );
        stores["clothing"] = new Store(
            "clothing", "warmth decay reduction",
            row + 1, offsetMultiplier + 2, posParams, textures
        );
        stores["flint"] = new Store(
            "flint", "fires!",
            row + 1, offsetMultiplier + 3, posParams, textures
        );
        stores["pants"] = new Store(
            "pants", "pants!",
            row + 1, offsetMultiplier + 4, posParams, textures
        );
        UpdateTrackingKeys();
    }

    public void UpdateTrackingKeys() {
        // get list of values entries
        List<string> valueKeys = values.Keys.ToList();
        // if store name is not in values dictionary, add it
        foreach(string name in storeNames) {
            if (!valueKeys.Contains(name)) {
                values.Add(name, 0);
            }
        }
        // get list of produced entries
        List<string> producedKeys = produced.Keys.ToList();
        // if store name is not in produced dictionary, add it
        foreach(string name in storeNames) {
            if (!producedKeys.Contains(name)) {
                produced.Add(name, 0);
            }
        }
        // get list of consumed entries
        List<string> consumedKeys = consumed.Keys.ToList();
        // if store name is not in consumed dictionary, add it
        foreach(string name in storeNames) {
            if (!consumedKeys.Contains(name)) {
                consumed.Add(name, 0);
            }
        }
    }

    public void Draw() {
        GUI.DrawTexture(outline, outlineTexture);
        foreach (KeyValuePair<string, Store> entry in stores) {
            entry.Value.Draw();
        }
    }

    public void Reset() {
        values = new Dictionary<string, float>();
        produced = new Dictionary<string, int>();
        consumed = new Dictionary<string, int>();
        SetDefaults();
    }

    public string GetStoresDebugRepr() {
        string theString = "STORES";
        theString += "\nQ: Quantity, P: Produced, C: Consumed";
        float totalStores = CountDictValues(values);
        float totalProduced = CountDictValues(produced);
        float totalConsumed = CountDictValues(consumed);
        foreach (string entry in storeNames) {
            theString += string.Format("\nQ:{0:R} - {1:R}% | P:{2:d} - {3:R}% | C:{4:d} - {5:R}% | {6:s}",
                values[entry],
                Mathf.Round((values[entry] / totalStores) * 100),
                produced[entry],
                Mathf.Round((produced[entry] / totalProduced) * 100),
                consumed[entry],
                Mathf.Round((consumed[entry] / totalConsumed) * 100),
                entry
            );
        }
        return theString;
    }

    private float CountDictValues(Dictionary<string, int> theDict) {
        float theSum = theDict.Sum(x => x.Value);
        return theSum;
    }

    private float CountDictValues(Dictionary<string, float> theDict) {
        float theSum = theDict.Sum(x => x.Value);
        return theSum;
    }

    public void Load() {
        UpdateTrackingKeys();
    }
}
